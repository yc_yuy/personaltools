#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: delSvnFolders.pl
#
#        USAGE: ./delSvnFolders.pl <folder> 
#
#  DESCRIPTION: I'm a git fun, so, .svn ...
#               It's a joke, when you wanna backup some source file, you don't 
#               want .svn be with it, this script can help you
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: YuYang (YY), yc2yuy@gmail.com
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 2012年08月07日 11时10分03秒
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;

# use File::Path qw(remove_tree); # will use this when porting to another platform like windows

our $Level = 0;

if(1 != @ARGV) {
    print "Please use : $0 <folder>\n";
    exit 1;
}

my $rootFolder = shift @ARGV;
print "Work on root folder : $rootFolder \n";

&delSvnFolder($rootFolder);

sub delSvnFolder(){
    my $curFolder = shift;

    &printWithLevel("-> Move into $curFolder");
    chdir $curFolder 
        or die "Cannot cd into $curFolder : $!";

    opendir my $CURDIR, '.'
        or die "Cannot open $curFolder : $!";

    foreach my $folder (readdir $CURDIR) {
        if (-d $folder){
            &printWithLevel("Find $folder");
            if('.svn' eq $folder){
            #if('CVS' eq $folder){ # CVS, of course
                &printWithLevel("Delete $folder");
                `rm -rf $folder`;
            } elsif('.' ne $folder && '..' ne $folder){
                ++$Level;
                &delSvnFolder($folder);
                --$Level;
            } else {
                &printWithLevel("Do nothing with $folder");
            }
        }
    }
    
    closedir $CURDIR;

    &printWithLevel("<- Move out $curFolder");
    chdir '..'
        or die "Cannot cd out $curFolder : $!";
}

sub printWithLevel(){
    my $line = shift;

    for (my $i = 0; $i < $Level; ++$i){
        print '--';
    }

    print "$line\n";
}

#!/usr/bin/env perl 
#===============================================================================
#
#         FILE: downloadDXJW.pl
#
#        USAGE: ./downloadDXJW.pl  
#
#  DESCRIPTION: 下载大兴教委留言回复
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: YuYang (YY), yc2yuy@gmail.com
# ORGANIZATION: 
#      VERSION: 1.0
#      CREATED: 2012年12月06日 10时20分24秒
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use utf8;
binmode(STDIN, ':encoding(utf8)');
binmode(STDOUT, ':encoding(utf8)');
binmode(STDERR, ':encoding(utf8)');

use LWP::UserAgent;
use HTTP::Response;
use Encode;

my $total = 137;

if (@ARGV > 0) {
    $total = shift;
}

my $URL = 'http://jyj.dxedu.gov.cn/liuyan.asp?page=';
foreach(1..$total){
    &getUrl($URL . $_);
}

# -------------------------------------------------------------------------------
sub getUrl(){
    my $url = shift;

    my $browser = LWP::UserAgent->new();
    my $response = $browser->get($url);

    my @content = split /\n/, $response->content();

    my $lineCnt = 0;
    foreach(@content){
        chomp;
        $_ = decode('gb2312', $_);
        $_ =~ s/&nbsp;/ /g;
        $_ =~ s/&quot;/"/g;
        $_ =~ s/<br \/>/ /g;
        $_ =~ s/\r//g;
        $_ = &trim($_);
        if(/^[^<|].*$/){
            if (!/^密码|^办公邮箱|^发布日期|^文章分类|^发布单位|^共.*条留言/){

                my $marker = 'A : ';
                if(0 == $lineCnt % 2) {
                    $marker = "\nQ : ";
                }

                print "$marker$_\n";
                ++$lineCnt;
            }
        } elsif (/color:005681.*(20\d{2,2}\/\d{1,2}\/\d{1,2})/) {
            print "[$1]\n";
        }
    }
}

sub trim(){
    my $str = shift;

    $str =~ s/^\s+//;
    $str =~ s/\s+$//;

    return $str;
}
